// Generated using webpack-cli https://github.com/webpack/webpack-cli

const path = require('path');
var glob = require('glob');

const RemoveEmptyScriptsPlugin = require('webpack-remove-empty-scripts');
const MiniCssExtractPlugin = require('mini-css-extract-plugin');

const isProduction = process.env.NODE_ENV == 'production';
const projectName = 'dwwm4';
const pathTheme = `themes/${projectName}/assets`;

const jsFiles = glob.sync(`${pathTheme}/src/js/*.js`);
const scssFiles = glob.sync(`${pathTheme}/src/scss/!(_)*.scss`);
const files = jsFiles
	.concat(scssFiles)
	.map((item) => {
		return path.resolve(__dirname, item);
	})
	.reduce((previous, current, i) => {
		const splitedName = current.split('/');
		const lastItem = splitedName[splitedName.length - 1];
		previous[lastItem.split('.')[0]] = current;
		return previous;
	}, {});
console.log(files);

const config = {
	//entry: path.resolve(__dirname, `${pathTheme}/src/js/app.js`),
	entry: files,
	output: {
		filename: 'js/[name].js',
		path: path.resolve(__dirname, `${pathTheme}/dist`),
	},
	plugins: [
		new RemoveEmptyScriptsPlugin(),
		new MiniCssExtractPlugin({
			filename: 'css/[name].css',
		}),
		// Add your plugins here
		// Learn more about plugins from https://webpack.js.org/configuration/plugins/
	],
	module: {
		rules: [
			{
				test: /\.(js|jsx)$/i,
				loader: 'babel-loader',
			},
			{
				test: /\.s[ac]ss$/i,
				use: [
					MiniCssExtractPlugin.loader,
					{
						loader: 'css-loader',
						options: {
							url: false,
						},
					},
					'postcss-loader',
					'sass-loader',
				],
			},
			// Add your rules for custom modules here
			// Learn more about loaders from https://webpack.js.org/loaders/
		],
	},
};

module.exports = () => {
	if (isProduction) {
		config.mode = 'production';
	} else {
		config.mode = 'development';
	}
	return config;
};